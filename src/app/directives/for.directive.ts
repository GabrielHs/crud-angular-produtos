import { Directive, OnInit, Input, ViewContainerRef, TemplateRef } from '@angular/core';

@Directive({
  selector: '[appFor]'
})
export class ForDirective implements OnInit {

  @Input('appForEm') numbers: Number[]
  @Input('appForUsando') texto: string

  constructor(private container: ViewContainerRef, private template: TemplateRef<any>) {

  }

  ngOnInit(): void {
    console.log(this.numbers);
    console.log(this.texto);

  }

}
