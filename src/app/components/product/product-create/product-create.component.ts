import { Product } from './../product.model';
import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {

  public product: Product = {
    name: "",
    price: null
  }

  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
  }

  cancel(): void {
    this.router.navigate(['/products'])
  }

  createProduct(): void {
    this.productService.create(this.product).subscribe((retorno: any) => {
      // console.log(retorno);

      this.productService.showMessage("Produto criado")
      
      this.cancel()
    }, (erro: any) => {
      this.productService.showMessage("Ocorreu um erro ao salvar" + erro)

    })

  }
}
