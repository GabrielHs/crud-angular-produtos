import { Product } from './product.model';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  public baseUrl = "http://localhost:3000/products";
  constructor(private snackBar: MatSnackBar, private http: HttpClient) { }


  showMessage(msg: string, isErro: boolean = false): void {
    this.snackBar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: 'top',
      panelClass: isErro ?  ['msg-error'] :  ['msg-sucess']
    })
  }

  create(product: Product): Observable<Product> {
    return this.http.post<Product>(this.baseUrl, product).pipe(
      map(obj => obj),
      catchError(e => this.errorHander(e))
    );
  }


  getProducts(): Observable<Product[]> {

    return this.http.get<Product[]>(this.baseUrl).pipe(
      map(obj => obj),
      catchError(e => this.errorHander(e))
    );;
  }

  getById(id: number): Observable<Product> {
    const url = `${this.baseUrl}/${id}`
    return this.http.get<Product>(url).pipe(
      map(obj => obj),
      catchError(e => this.errorHander(e))
    );
  }

  update(product: Product): Observable<Product> {
    const url = `${this.baseUrl}/${product.id}`

    return this.http.put<Product>(url, product).pipe(
      map(obj => obj),
      catchError(e => this.errorHander(e))
    );
  }

  delete(id: number): Observable<Product> {
    const url = `${this.baseUrl}/${id}`

    return this.http.delete<Product>(url).pipe(
      map(obj => obj),
      catchError(e => this.errorHander(e))
    );
  }


  errorHander(e: any): Observable<any>{
    this.showMessage('Ocorreu um erro', true)
    return EMPTY
  }
}
