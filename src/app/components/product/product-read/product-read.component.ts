import { ProductService } from './../product.service';
import { Product } from './../product.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-read',
  templateUrl: './product-read.component.html',
  styleUrls: ['./product-read.component.css']
})
export class ProductReadComponent implements OnInit {
  public productcs: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.loadList()
  }

  loadList(): void {
    this.productService.getProducts().subscribe((dados) => {
      this.productcs = dados

    }, (erro: any) => {
      this.productService.showMessage("Ocorreu um erro listar dados" + erro)

    }
    )

  }

}
