import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from '../product.model';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {

  constructor(private productService: ProductService, private router: Router, private route: ActivatedRoute) { }

  public product =  new Product()

  ngOnInit(): void {
    this.loadProduct()
  }

  loadProduct() {
    
    let id = Number(this.route.snapshot.paramMap.get("id"))
    this.productService.getById(id).subscribe(
      (prod: Product) => {
        this.product = prod
      },
      (error) => {
        this.productService.showMessage("Ocorreu um erro ao carregar" + error)

      }
    )

  }

  updateProduct(): void {
    this.productService.update(this.product).subscribe(
      () => {
        this.productService.showMessage("Produto atualizado com sucesso")
        this.router.navigate(["/products"])
      }
    )
  }

  cancel(): void {
    this.router.navigate(['/products'])
  }

}
