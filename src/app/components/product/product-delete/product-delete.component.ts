import { Component, OnInit } from '@angular/core';
import { Product } from '../product.model';
import { ProductService } from '../product.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-delete',
  templateUrl: './product-delete.component.html',
  styleUrls: ['./product-delete.component.css']
})
export class ProductDeleteComponent implements OnInit {

  public product =  new Product()

  constructor(private productService: ProductService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadProduct()
  }

  loadProduct() {
    
    let id = +this.route.snapshot.paramMap.get("id")
    this.productService.getById(id).subscribe(
      (prod: Product) => {
        this.product = prod
      },
      (error) => {
        this.productService.showMessage("Ocorreu um erro ao carregar" + error)

      }
    )

  }

  cancel(): void {
    this.router.navigate(['/products'])
  }

  deleteProduct(): void {
    this.productService.delete(this.product.id).subscribe(
      () => {
        this.productService.showMessage("Produto excluido com sucesso")
        this.router.navigate(["/products"])
      }
    )
  }
}
